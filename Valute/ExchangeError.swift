//
//  ExchangeError.swift
//  Valute
//
//  Created by Nemanja Gagic on 5/22/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import Foundation

enum ExchangeError: Error {
    case networkError(originalError: Error)
    case invalidResponse
    case missingRate
    case invalidCurrencyCode(cc: String)
    
    
    var title: String? {
        switch self {
        case .networkError:
            return nil
        case .invalidResponse:
            return nil
        case .missingRate:
            return nil
        case .invalidCurrencyCode:
            return NSLocalizedString("Unknown currency", comment: "")
        }
        return nil
    }
    
    var message: String? {
        switch self {
        case .networkError(let originalError):
            return originalError.localizedDescription
            // (ili (originalError as! NSError).localizedDescription
        case .invalidResponse:
            return NSLocalizedString("Invalid response", comment: "")
        case .missingRate:
            return NSLocalizedString("Missing rate for given currencies", comment: "")
        case .invalidCurrencyCode(let cc):
            return String(format: NSLocalizedString("Invalid currency code: %@", comment: "%@ is a marker for used currency code"), cc)
        }
        return nil
    }
}











