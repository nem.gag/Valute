//
//  Formatter.swift
//  Valute
//
//  Created by Nemanja Gagic on 5/3/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import Foundation

// singleton instanca NumberFormatter klase
// kreira se staticni Type Property "moneyFormatter" od tima NumberFormatter
// napravi se closure i izvrsi " = { ... return }() "
// interno se uradi var nf = NumberFormatter() i return nf, a izmedju se setuju karakteristike
// NumberFormatter ima oko 60 property-a, a nama treba generatesDecimalNumbers (true), maximumFractionDigits (2) i minimumFractionDitigs (2)
// sto se tice "." ili "," bice ono sto je korisnik setovao (Locale.current)

extension NumberFormatter {
    
    static let stringFormatter: NumberFormatter = {
        var nf = NumberFormatter()
        nf.generatesDecimalNumbers = true
        nf.minimumFractionDigits = 2
        nf.maximumFractionDigits = 2
        return nf
    }()
    
    static let decimalFormatter: NumberFormatter = {
        var nf = NumberFormatter()
        nf.generatesDecimalNumbers = true
        nf.numberStyle = .decimal
        return nf
    }()
    
    // extension for stringString and amountDecimal
    static func amountDecimal(str: String) -> Decimal! {
        return NumberFormatter.stringFormatter.number(from: str)?.decimalValue
    }
    
    static func amountString(num: Decimal) -> String? {
        return NumberFormatter.decimalFormatter.string(for: num)
    }


}



















