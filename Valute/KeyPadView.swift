//
//  KeyPadView.swift
//  Valute
//
//  Created by Nemanja Gagic on 5/3/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit

// MARK:- Protocol delegate
protocol KeypadViewDelegate: class {
    func keypadView(_ keypad: KeypadView, didChangeAmount value: Decimal?)
    func keypadView(_ keypad: KeypadView, didChangeValue value: String?)
}

// MARK:- View Outlets
final class KeypadView: UIView {
    
    weak var delegate: KeypadViewDelegate?
    
    @IBOutlet var digitButtons: [UIButton]!
    @IBOutlet var operatorButtons: [UIButton]!
    @IBOutlet weak var decimalButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var equalsButton: UIButton!
    
    var allButtons: [UIButton] {
        return digitButtons + operatorButtons + [decimalButton, decimalButton, equalsButton]
    }
    
    // MARK:- Internal Data Types
    fileprivate enum ArithmeticOperation {
        case add
        case subtract
        case multiply
        case divide
        case equals
    }
    
    // MARK:- Internal Data Model
    fileprivate var originalBackgroundColor: UIColor?
    fileprivate var firstOperand: Decimal?
    fileprivate var secondOperand: Decimal?
    fileprivate var operation: ArithmeticOperation?
    
    // MARK:- External Data Model
    fileprivate(set) var stringAmount: String? {
        // MARK:- Delegate trigger
        didSet {
            delegate?.keypadView(self, didChangeValue: stringAmount)
            delegate?.keypadView(self, didChangeAmount: decimalAmount)
        }
    }
    
    var decimalAmount: Decimal? {
        guard let str = stringAmount else { return nil }
        return NumberFormatter.amountDecimal(str: str)
    }
}


// MARK:- View Lifecycle Methods
extension KeypadView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        configureButtonsTouch()
        configureButtonsUntouch()
        configureButtonsTap()
        configureDecimalButton()
        prepareDisplay()
    }
}

// MARK:- UI Setup: Buttons configuration
fileprivate extension KeypadView {
    
    // target-action pattern for buttonTouch -> TouchDown
    func configureButtonsTouch() {
        for btn in allButtons {
            btn.addTarget(self, action: #selector(KeypadView.didTouchButton), for: .touchDown)
        }
    }
    
    // target-action pattern for buttonUntouch -> TouchCancel and TouchUpOutside
    func configureButtonsUntouch() {
        for btn in allButtons {
            btn.addTarget(self, action: #selector(KeypadView.didUntouchButton), for: .touchCancel)
            btn.addTarget(self, action: #selector(KeypadView.didUntouchButton), for: .touchUpOutside)
        }
    }
    
    // local configuration for display of decimalButton
    func configureDecimalButton() {
        decimalButton.setTitle(Locale.current.decimalSeparator, for: .normal)
    }

    // target-action pattern for TouchDown
    func configureButtonsTap() {
        for btn in digitButtons {
            btn.addTarget(self, action: #selector(KeypadView.didTapDigit), for: .touchUpInside)
        }
        
        let operators = operatorButtons + [equalsButton]
        for btn in operators {
            btn.addTarget(self, action: #selector(KeypadView.didTapOparator), for: .touchUpInside)
        }
    }
    
    func prepareDisplay() {
        equalsButton.alpha = 0
    }
}

// MARK:- Actions, Event Handler: didTouchButtons, didUntouchButtons
extension KeypadView {
    
    func didTouchButton(_ sender: UIButton) {
        originalBackgroundColor = sender.backgroundColor
        
        guard let _ = sender.backgroundColor else {
            sender.backgroundColor = UIColor.black.withAlphaComponent(0.2)
            return
        }
        
        var r : CGFloat = 0
        var g : CGFloat = 0
        var b : CGFloat = 0
        
        var a : CGFloat = 0.2
        
        guard let _ = sender.backgroundColor?.getRed(&r, green: &g, blue: &b, alpha: &a)
            else {
                sender.backgroundColor = UIColor.black.withAlphaComponent(0.2)
                return
        }
        sender.backgroundColor = UIColor(red: r, green: g, blue: b, alpha: a)
    }

    func didUntouchButton(_ sender: UIButton) {
        sender.backgroundColor = originalBackgroundColor
        originalBackgroundColor = nil
    }
}


// MARK:- Actions, Event Handler: didTapDigit, didTapOperator
extension KeypadView {

    func didTapDigit(_ sender: UIButton) {
        
        defer { didUntouchButton(sender) }

        guard let numString = sender.title(for: .normal) else { return }
        
        var value = stringAmount ?? ""
        value += numString
        stringAmount = value
    }
    
    func didTapOparator(_ sender: UIButton) {
        
        defer { didUntouchButton(sender) }
        
        var isEquals = false
        
        guard let caption = sender.title(for: .normal) else {
            fatalError("Received operator button tap from button with no caption on it")
        }
        
        switch caption {
        case "+":
            operation = .add
        case "-":
            operation = .subtract
        case "x":
            operation = .multiply
        case "÷":
            operation = .divide
        case "=":
            isEquals = true
        default:
            operation = nil
        }
        
        
        if isEquals {
            guard let str = stringAmount, let num = NumberFormatter.amountDecimal(str: str) else { return }
            
            guard var result = firstOperand else {
                fatalError("Missing first operand!")
            }
            
            switch operation! {
            case .add:
                result += num
            case .subtract:
                result -= num
            case .multiply:
                result = result * num
            case .divide:
                result = result / num
                // *= and /= does not work with decimals
            default:
                return
            }
            
            stringAmount = NumberFormatter.decimalFormatter.string(for: result)
            operation = nil
            
            UIView.animate(withDuration: 0.3, animations: {
                [unowned self] in
                for btn in self.operatorButtons {
                    btn.alpha = 1
                }
                self.equalsButton.alpha = 0
            })
            
        } else if let _ = operation {
            
            guard let str = stringAmount, let num = NumberFormatter.amountDecimal(str: str) else { return }
            
            firstOperand = num
            
            stringAmount = nil
            
            UIView.animate(withDuration: 0.3, animations: {
                [unowned self] in
                for btn in self.operatorButtons {
                    btn.alpha = 0
                }
                self.equalsButton.alpha = 1
            })
        }
    }
}


// MARK:- Actions, Event Handler: didTapDecimalButton, didTapDeleteButton
extension KeypadView {
    
    @IBAction func didTapDecimalButton(_ sender: UIButton) {
        
        defer { didUntouchButton(sender) }
        
        guard let dot = sender.title(for: .normal) else { return }
        
        var value = stringAmount ?? ""
        
        if !value.contains(dot) {
            value += dot
            stringAmount = value
        }
    }
    
    @IBAction func didTapDeleteButton(_ sender: UIButton) {
        
        defer { didUntouchButton(sender) }

        guard let str = stringAmount, str.characters.count > 0 else { return }
        
        let s = str.substring(to: str.index(before: str.endIndex))
        
        stringAmount = s
    }
}



















