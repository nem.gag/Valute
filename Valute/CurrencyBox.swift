//
//  CurrencyBox.swift
//  Valute
//
//  Created by Nemanja Gagic on 5/3/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit

// MARK:- Delegate protocol
protocol CurrencyBoxDelegate: class {
    func currencyBoxRequestsCurrencyChange(_ box: CurrencyBox)
}


// MARK:- Internal Data Model
final class CurrencyBox: UIView {
    
    weak var delegate: CurrencyBoxDelegate?
    
    @IBOutlet fileprivate weak var currencyCodeLabel: UILabel!
    @IBOutlet fileprivate weak var flagImageView: UIImageView!
    @IBOutlet fileprivate weak var textField: UITextField!
}


// MARK:- Delegate trigger
fileprivate extension CurrencyBox {
    
    @IBAction func didTapButton(_ sender: UIButton) {
        delegate?.currencyBoxRequestsCurrencyChange(self)
    }
}

extension CurrencyBox {

    var currencyCode: String {
        get {
            return currencyCodeLabel.text!
        }
        set {
            currencyCodeLabel.text = newValue
            let cc = Locale.countryCode(for: newValue)
            let img = UIImage(named: cc) ?? #imageLiteral(resourceName: "empty")
            flagImageView.image = img
        }
    }
    
    var amountDecimal: Decimal? {
        get {
            return NumberFormatter.amountDecimal(str: textField.text!) ?? nil
        }
        set {
            textField.text = NumberFormatter.stringFormatter.string(for: newValue)
        }
    }
    
    var amountString: String? {
        get {
            return textField.text
        }
        set {
            textField.text = newValue
        }
    }
}

extension CurrencyBox: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return false
    }
}







