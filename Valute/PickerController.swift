//
//  PickerController.swift
//  Valute
//
//  Created by Nemanja Gagic on 5/5/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit

// MARK:- Delegator side of Delegate implementation, UI Setup
protocol PickerControllerDelegate: class {
    func pickerController(_ controller: PickerController, didSelect currencyCode: String)
}

final class PickerController: UIViewController, StoryboardLoadable {
    
    @IBOutlet fileprivate weak var tableView: UITableView!

    weak var delegate: PickerControllerDelegate?
    
    var currencies: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareDataSource()
        // navigationItem.hidesBackButton = true
    }
}


// MARK:- TableView configuration, data source
extension PickerController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cc = currencies[indexPath.row]
        let cell: CurrencyCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.configure(with: cc)
        return cell
    }
}


// MARK:- TableView configuration, delegate
extension PickerController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cc = currencies[indexPath.row]
        delegate?.pickerController(self, didSelect: cc)
    }
}


// MARK:- Currencies configuration
extension PickerController {
    func prepareDataSource() {
        currencies = ExchangeManager.shared.allowedCurrencies
    }
}











