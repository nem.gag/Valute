//
//  ConvertController.swift
//  Valute
//
//  Created by Nemanja Gagic on 4/26/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit

// MARK:- Internal Data Model
final class ConvertController: UIViewController, StoryboardLoadable {
    
    @IBOutlet weak var sourceCurrencyBox: CurrencyBox!
    @IBOutlet weak var targetCurrencyBox: CurrencyBox!
    @IBOutlet weak var keypadView: KeypadView!
    
    fileprivate enum Key: String {
        case sourceCC = "sourceCurrencyCode"
        case targetCC = "targetCurrencyCode"
    }
    
    // MARK: User defaults
    fileprivate var sourceCurrencyCode: String = "" {
        didSet {
            UserDefaults.standard.set(sourceCurrencyCode, forKey: Key.sourceCC.rawValue)
            sourceCurrencyBox.currencyCode = sourceCurrencyCode
            // update UI, if activeCurrencyBox == sourceCurrencyBox { sourceCurrencyCode = currencyCode }
        }
    }
    fileprivate var targetCurrencyCode: String = "" {
        didSet {
            UserDefaults.standard.set(targetCurrencyCode, forKey: Key.targetCC.rawValue)
            targetCurrencyBox.currencyCode = targetCurrencyCode
            // update UI, if activeCurrencyBox == targetCurrencyBox { targetCurrencyCode = currencyCode }
        }
    }
    
    
    fileprivate var activeCurrencyBox: CurrencyBox?
    
    fileprivate var amountDecimal: Decimal? {
        didSet {
            updateConversionPanel()
        }
    }
}

// MARK:- View lifecycle
extension ConvertController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Currency Converter"
        
        configureCurrencyBoxes()
        configureKeypad()
        populateDataModel()
    }
}

// MARK:- Delegate side of CurrencyBox Delegate
extension ConvertController: CurrencyBoxDelegate {
    
    fileprivate func configureCurrencyBoxes() {
        sourceCurrencyBox.delegate = self
        targetCurrencyBox.delegate = self
        
        sourceCurrencyBox.amountDecimal = nil
        targetCurrencyBox.amountDecimal = nil
    }
    
    func currencyBoxRequestsCurrencyChange(_ box: CurrencyBox) {
        
        let vc = PickerController.instantiate(fromStoryboardNamed: "Convert")
        vc.delegate = self
        vc.currencies = ExchangeManager.shared.allowedCurrencies
        show(vc, sender: self)
        
        activeCurrencyBox = (box == sourceCurrencyBox) ? sourceCurrencyBox : targetCurrencyBox
        
        updateConversionPanel()
    }
}


// MARK:- Delegate side of PickerController Delegate
extension ConvertController: PickerControllerDelegate {
    
    func pickerController(_ controller: PickerController, didSelect currencyCode: String) {
        self.navigationController?.popViewController(animated: true)
        
        // update data model
        guard activeCurrencyBox != nil else { return }
        
        if activeCurrencyBox == sourceCurrencyBox {
            sourceCurrencyCode = currencyCode
        } else {
            targetCurrencyCode = currencyCode
        }
        
        resetConversionPanel()
        
        self.activeCurrencyBox = nil
    }
}


// MARK:- Delegate side of KeypadView Delegate
extension ConvertController: KeypadViewDelegate {
    
    fileprivate func configureKeypad() {
        keypadView.delegate = self
    }
    
    func keypadView(_ keypad: KeypadView, didChangeAmount value: Decimal?) {
        amountDecimal = value
    }
    
    func keypadView(_ keypad: KeypadView, didChangeValue value: String?) {
        sourceCurrencyBox.amountString = value
    }
}


// MARK:- Populate data model and update conversion panel
fileprivate extension ConvertController {
    
    func populateDataModel() {
        
        sourceCurrencyCode = (UserDefaults.standard.value(forKey: Key.sourceCC.rawValue) as? String) ?? sourceCurrencyBox.currencyCode
        targetCurrencyCode = (UserDefaults.standard.value(forKey: Key.targetCC.rawValue) as? String) ?? targetCurrencyBox.currencyCode
        
        amountDecimal = sourceCurrencyBox.amountDecimal
    }
    
    func updateConversionPanel() {
        
        guard let amount = amountDecimal else { return }
        
        ExchangeManager.shared.conversionRate(from: sourceCurrencyCode, to: targetCurrencyCode) {
            // CACHE: to store downloaded rates and use them again.
            
            [weak self] rate, error in
            guard let `self` = self else { return }
            
            if let error = error {
                DispatchQueue.main.async {
                    let ac = UIAlertController(title: error.title, message: error.message, preferredStyle: .alert)
                    let ok = UIAlertAction(title: NSLocalizedString("OK", comment: ""),
                                           style: .default)
                    ac.addAction(ok)
                    self.present(ac, animated: true, completion: nil)
                }
                return
            }
            
            guard let rate = rate else { return }

            let result = amount * rate
            
            DispatchQueue.main.async {
                self.targetCurrencyBox.amountDecimal = result
            }
        }
    }
    
    func resetConversionPanel() {
        
        ExchangeManager.shared.conversionRate(from: sourceCurrencyCode, to: targetCurrencyCode) {
            [weak self] rate, _ in
            guard let rate = rate else {return }
            
            DispatchQueue.main.async {
                self?.targetCurrencyBox.amountDecimal = rate
            }
        }
    }
}


















