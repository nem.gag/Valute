//
//  CurrencyCell.swift
//  Valute
//
//  Created by Nemanja Gagic on 5/8/17.
//  Copyright © 2017 Nemanja Gagic. All rights reserved.
//

import UIKit

final class CurrencyCell: UITableViewCell, NibReusableView {

    @IBOutlet fileprivate weak var label: UILabel!
    @IBOutlet fileprivate weak var flagImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    func configure(with currencyCode: String) {
        
        label.text = currencyCode
        
        let cc = Locale.countryCode(for: currencyCode)
        let img = UIImage(named: cc) ?? UIImage(named: "empty")
        
        flagImageView.image = img
    }
}
